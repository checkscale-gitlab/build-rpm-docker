Name: Sample
Version: 1
Release: 1
Summary: Sample RPM
BuildArch: noarch
License: GPL
URL: http://...
Source0: README.md

%description
Sample noarch rpm

%install
mkdir -p %{buildroot}/opt/sample
install -m 644 %SOURCE0 %{buildroot}/opt/sample/

%files
%defattr(-, root, root)
%dir /opt/sample
/opt/sample/README.md
#%attr(755,root,root) /opt/sample/README.md
