# Build RPM Docker

***A minimal Docker Image for building RPMs directly from RPM spec files***


## Building the Image

For convenience a makefile is provided in the project to build the Docker Image and upload it to DockerHub

**Build Container:**

`make build`

**Upload Image to Docker Registry:**

`make upload`
