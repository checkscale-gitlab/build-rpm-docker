# Makefile for Minimal Docker RPM Builder
# Build Image: make build
# Upload Image: make upload
# Login to DockerHub: make login

NAME := "build-rpm-docker"
DOCKER_REGISTRY := "hub.docker.com"
TAG := 1
USER := $(shell id -u)
GROUP := $(shell id -g)

.PHONY: build upload clean

build:
	docker build --tag "$(DOCKER_REGISTRY)/$(NAME):$(TAG)" .

upload: login build
	docker push "$(DOCKER_REGISTRY)/$(NAME):$(TAG)"

login:
	docker login "$(DOCKER_REGISTRY)""

clean:
	docker rm -v `docker ps -a -q  -f status=exited -f name=$(NAME)`

rpm: build
	docker run -it -v "${PWD}":"/build" -e USER=$(USER) -e GROUP=$(GROUP) "$(DOCKER_REGISTRY)/$(NAME):$(TAG)"
